var program = require('commander');
var http = require('http');
var https = require('https');
var fs = require('fs');
var unzip = require('unzip');
var rmdir = require('rmdir');
var open = require('open');
var colors = require('colors');
var glob = require("glob")
var contents = fs.readFileSync('index.html', 'utf8');
var fs = require('fs');
var path = require('path');
var appDir = path.dirname(require.main.filename);
var plg_com = process.argv.slice(3);
var css_f,js_f,link_d;
var css_p = '/lib/data/css';
var js_p = '/lib/data/js';
var css= '<link href="css/screen.css" rel="stylesheet" />';
var css_2= '<link href="css/style.css" rel="stylesheet" />';
var head= '</head>';
var js= '</body>';
var script = '<script src="js/main.js"></script>';
var exit = require('exit');

/* Get Plugin from Json */
var file = require('../lib/plg.json');
fs.readFile(appDir+'/lib/plg.json', 'utf8', function (err, data) {
    if (err) {
        console.log('Error: ' + err);
        return;
    }
    try{
        data = JSON.parse(data);
        css_f = data[plg_com].css;
        js_f = data[plg_com].js;
        link_d = data[plg_com].link;
        script_l = data[plg_com].script;
        console.log('Further Detail of Plugin \r\n');
        console.log('link/detail:'.yellow+link_d.green);
    }
    catch(e){
        console.log("Invalid Plugin")
        console.log("This Plugin Not in a List")
        exit()
    }

});

/* Add CSS in Index File */
fs.readFile('index.html', 'utf8', function (err,data) {
    if (err) 
    {
        console.log("Error")
        fs.writeFile('index.html', contents, 'utf8')
        exit()
    }
    if(data.indexOf(css)>0)
    {
        if(css_f!=""){
            fs.createReadStream(appDir+'/'+css_p+'/'+css_f+'.css').on('error', function(e){
                console.error('No data in you plugin Directory'.red)
            }).pipe(fs.createWriteStream('./css/'+css_f+'.css').on('error', function(e){
                console.log('No css drectory found in you drectory'.red)
            })).on('error', function(e){
                console.log("can't transfer css files".red)
            });
            var result = data.replace(css, css +'\r\n <link href="css/'+css_f+'.css" rel="stylesheet" />')
            fs.writeFile('index.html', result, 'utf8', function (err) {
                if (err) 
                {
                    fs.writeFile('index.html', contents, 'utf8')
                    console.log("0")
                }
            });
        }
    }
    else
    {
        if(data.indexOf(css_2)>0)
        {
            if(css_f!=""){
                fs.createReadStream(appDir+'/'+css_p+'/'+css_f+'.css').on('error',function(e){
                    console.error('No data in you plugin Directory'.red)
                }).pipe(fs.createWriteStream('./css/'+css_f+'.css').on('error',function(e){
                    console.log('No css drectory found in you drectory'.red)
                })).on('error',function(e){
                    console.log("can't transfer css files".red)
                });
                var result = data.replace(css_2, css_2 +'\r\n <link href="css/'+css_f+'.css" rel="stylesheet" />')
                fs.writeFile('index.html', result, 'utf8', function (err) {
                    if (err) 
                    {
                        fs.writeFile('index.html', contents, 'utf8')
                        console.log("0")
                    }
                });
            }
        }
        else
        {
            if(css_f!=""){
                fs.createReadStream(appDir+'/'+css_p+'/'+css_f+'.css').on('error',function(e){
                    console.error('No data in you plugin Directory'.red)
                }).pipe(fs.createWriteStream('./css/'+css_f+'.css').on('error',function(e){
                    console.log('No css drectory found in you drectory'.red)
                })).on('error',function(e){
                    console.log("can't transfer css files".red)
                });
                var result = data.replace(head,'<link href="css/'+css_f+'.css" rel="stylesheet" />\r\n'+head)
                fs.writeFile('index.html', result, 'utf8', function (err) {
                    if (err) 
                    {
                        fs.writeFile('index.html', contents, 'utf8')
                        console.log("0")
                    }
                });
            }
        }
    }
})

/* Add Js in Index File */
setTimeout(function(){
    fs.readFile('index.html', 'utf8', function (err,data) {
        if (err) 
        {
            return console.log(err);
            fs.writeFile('index.html', contents, 'utf8')
            exit();
        }
        if(script_l !="")
        {
            fs.createReadStream(appDir+'/'+js_p+'/'+js_f+'.js').on('error',function(e){
                console.error('No data in you plugin Directory'.red)
            }).pipe(fs.createWriteStream('./js/'+js_f+'.js').on('error',function(e){
                console.log('No JS drectory found in you drectory'.red)
            })).on('error',function(e){
                console.log("can't transfer JS files".red)
            });
            var result = data.replace(js,  '<script src="js/'+js_f+'.js"></script>\r\n <script>'+script_l+'</script>\r\n'+js);
            fs.writeFile('index.html', result, 'utf8', function (err) {
                if (err) 
                {
                    return console.log(err);
                    fs.writeFile('index.html', contents, 'utf8')
                    exit();
                }
            });
        }
        else
        {
            fs.createReadStream(appDir+'/'+js_p+'/'+js_f+'.js').on('error',function(e){
                console.error('No data in you plugin Directory'.red)
            }).pipe(fs.createWriteStream('./js/'+js_f+'.js').on('error',function(e){
                console.log('No JS drectory found in you drectory'.red)
            })).on('error',function(e){
                console.log("can't transfer JS files".red)
            });
            var result = data.replace(js,  '<script src="js/'+js_f+'.js"></script>\r\n'+js);
            fs.writeFile('index.html', result, 'utf8', function (err) {
                if (err) 
                {
                    return console.log(err);
                    fs.writeFile('index.html', contents, 'utf8')
                    exit()
                }
            });
        }
    })
},500);
