var http = require('http');
var https = require('https');
var fs = require('fs');
var open = require('open');
var index = fs.readFileSync('index.html');
var colors = require('colors');
var watch = require('node-watch');
var glob = require("glob")

http.createServer(function (req, res) {
    res.writeHeader(200, {'Content-Type': 'text/html'});
    res.write(index);
    res.end();
}).listen(8081);
open('http://localhost:8081', function (err) {
    if (err) throw err;
    console.log('The user closed the browser');
});
glob("**", function (er, files) {
    watch(files, function(filename) {
        console.log(filename, 'changed.'.green);
        console.log(process.argv);
    });
})

