var colors = require('colors');
var fs = require('fs');
var path = require('path');
var appDir = path.dirname(require.main.filename);
var file = require('../lib/plg.json');
console.log('\n  ------- Smart Code -------'.yellow);
console.log('\n  ---------------------------------------------------------------\n'.blue);
console.log('     ********   *********  **********  **********   ********'.blue);
console.log('     *          *          *        *    *      *   *'.blue);
console.log('     *          *          *        *    *      *   *'.blue);
console.log('     *          *          *        *    *      *   *'.blue);
console.log('     ********   *          *        *    *      *   ********'.blue);
console.log('            *   *          *        *    *      *   *'.blue);
console.log('            *   *          *        *    *      *   *'.blue);
console.log('            *   *          *        *    *      *   *'.blue);
console.log('     ********   *********  **********  **********   ********'.blue);
console.log('\n   ---------------------------------------------------------------\n'.blue);
console.log("   ------------------Arguments------------------ \n")
console.log("   For create Default strcture of Front-end system\n".yellow)
console.log("   scode init dir(Folder Name)".green)
console.log("   scode plg plgName".green)
console.log("   scode upload".green)
console.log("   scode help\n".green)
console.log("   plugin Lists".green)
fs.readFile(appDir+'/lib/plg.json', 'utf8', function (err, data) {
    if (err) {
        console.log('Error: ' + err);
        return;
    }
    data = JSON.parse(data);
    for(var key in data)
    { 
        console.log('     - '+key.green);
    }
});