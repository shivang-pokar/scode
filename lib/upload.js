var colors = require('colors');
var fs = require('fs');
var path = require('path');
var appDir = path.dirname(require.main.filename);
var prompt = require('prompt');
var JsonDB = require('node-json-db');
var db = new JsonDB(appDir+"/lib/plg.json", true, false);
var http = require('http');
var https = require('https');
var css_p = '/lib/data/css';
var js_p = '/lib/data/js';
var schema = {    
    properties: {     
        Name_plugin: {
            message: 'Enter Plugin Name',
            required: true   
        },
        css: {
            pattern:/^(?:(?:https?|ftp):\/\/)(?:\S+(?::\S*)?@)?(?:(?!10(?:\.\d{1,3}){3})(?!127(?:\.\d{1,3}){3})(?!169\.254(?:\.\d{1,3}){2})(?!192\.168(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]+-?)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]+-?)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/[^\s]*)?$/i,
            message: 'Enter Link of CSS'
        },
        js:{
            pattern:/^(?:(?:https?|ftp):\/\/)(?:\S+(?::\S*)?@)?(?:(?!10(?:\.\d{1,3}){3})(?!127(?:\.\d{1,3}){3})(?!169\.254(?:\.\d{1,3}){2})(?!192\.168(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]+-?)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]+-?)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/[^\s]*)?$/i,
            message: 'Enter Link of JS',
            required: true
        },
        link:{
            message:'Enter Detail or Url of Plugin '
        },
        script:{
            message:'Enter Script Here'
        }
    }  
};

/*db.push("/shivang", {css:"css",js:"js"});*/
prompt.get(schema, function (err, result) {       
    console.log('\n\nYour Plugin Command is:'.yellow);   
    try
    {
        var file = fs.createWriteStream(appDir+'/'+css_p+'/'+result.Name_plugin+'.css')
        var request = http.get(result.css, function(response) {
            response.pipe(file);
            file.on('finish', function() {

            });
        }).on('error',function(e){
            console.log('Check your net connection')
            console.log('Any Network Issue')
            console.log('You did not put css')
            fs.unlink(appDir+'/'+css_p+'/'+result.Name_plugin+'.css')
        });
    }
    catch(e){
        console.log("May be Error or You did't put CSS link".red)
    }
    var file_1 = fs.createWriteStream(appDir+'/'+js_p+'/'+result.Name_plugin+'.js')
    try
    {
        var request = http.get(result.js, function(response) {
            response.pipe(file_1);
            file_1.on('finish', function() {
                if(result.css == "" || result.css == null)
                {
                    db.push('/'+result.Name_plugin, {css:"",js:result.Name_plugin,link:result.link,script:result.script});
                }
                else
                {
                    db.push('/'+result.Name_plugin, {css:result.Name_plugin,js:result.Name_plugin,link:result.link,script:result.script});
                }
                console.log('scode plg ' + result.Name_plugin);
            });
        }).on('error',function(e){
            console.log('Check your net connection')
            console.log('Any Network Issue')
            fs.unlink(appDir+'/'+js_p+'/'+result.Name_plugin+'.js')
        });
    }
    catch(e){
        console.log("May be Error or You did't put CSS link".red)
    }
});