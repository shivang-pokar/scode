var folder = process.argv.slice(2);
var http = require('http');
var https = require('https');
var fs = require('fs');
var unzip = require('unzip');
var rmdir = require('rmdir');
var ProgressBar = require('progress');

var dir = folder[1]; 
if(dir == null)
{
    console.log("Please Enter Folder Name");
    console.log("scode init example");
}
else
    if (!fs.existsSync(dir))
    {
        fs.mkdirSync(dir);
        var request = https.get("https://gitlab.com/sivu503/default/repository/archive.zip?ref=master", function(response) {
            var file = fs.createWriteStream(dir +"/html.zip");
            response.pipe(file);
            file.on('finish', function() {
                fs.createReadStream(dir +"/html.zip").pipe(unzip.Extract({ path: dir }));
                fs.unlink(dir +"/html.zip")
                setTimeout(function(){
                    fs.rename(dir +'/default-master-93fe92b6b90872813faf9b1a5634ddb3541ae728', dir +'/html', function (err) {
                        if (err) throw err;
                    });
                },1000)
            });
        }).on('error',function(e){
            console.log('Check your net connection')
            console.log('Any Network Issue');
            fs.unlink(dir)
        }).on('response', function(response){
            var len = parseInt(response.headers['content-length'], 10);
            try{
                var bar = new ProgressBar('downloading [:bar] :percent :etas', {
                    complete: '=',
                    incomplete: ' ',
                    width: 20,
                    total:len 
                });
                response.on('data', function (chunk) {
                    bar.tick(chunk.length);
                });
                response.on('end', function () {
                    console.log('Download completed Enjoy :)');
                });
            }
            catch(e){
                console.log('Network Error')
                fs.unlink(dir);
                console.log(e)
            }
        });
        request.end();
    }
else
{
    console.log("Folder already existes")
}