var program = require('commander');
var http = require('http');
var https = require('https');
var fs = require('fs');
var unzip = require('unzip');
var rmdir = require('rmdir');
var path = require('path');
var ProgressBar = require('progress');
var colors = require('colors');
var com;
var argu = 'scode_i';
// index.js
require('shelljs/global');
var shell = require('shelljs');


// Root Derectory 
program
.version('1.0.0')
.option('-init',"To create Default Strcture of front-end")
.option('-plg',"To add external plugins ('like slider,date-picker,etc')")
.option('-upload',"To uplod your own plugins")
.option('-help',"To uplod your own plugins")
program
.command('plg [env]')
.description('run setup commands for all envs')
.action(function(env){
    argu = env;
    try
    {
        com = require('../lib/plg')
    }
    catch(e)
    {
        console.log("\n Check you in correct Dir \n may be not found index page check at once \n ".red)
        console.log(" scode --help".red)
    }
});
program
.command('*')
.action(function(env){
    env = env || 'all';
    argu = env;
    if(argu != "all")
    {
        try
        {
            com = require('../lib/'+argu)
        }
        catch(e)
        {
            console.log("Arument not available check again".red)
            console.log("scode --help".red)
        }
    }
    else
        console.log('you Enter Wrong Arument'.red);
});
program.parse(process.argv);
if(argu == "scode_i")
{
    com = require('../lib/'+argu);
}