<p><img src="http://shivang.comuf.com/scode/sc.jpg" alt="Smart Code" width="290"/></p>
<h2><a href="https://gitlab.com/sivu503/scode"></a></h2>
<h2>About</h2>
<p>This package is for Frontend developer, Here they can use my structure Template also js plugins which are already in this package also if you want you can add your own js plugins with single command with the help of this this plugins you dont's need to place css and js every time in your code </p>
<h2>Features</h2>
<ul>
<li>
<strong>Can use html Structure template with one command </strong>
</li>
<li>
Almost plugins are already in package like Slider, Date-picker ,Fullpage-js ,Custome Scroll , smoothscroll , etc...  
</li>
<li>
Add plugins your own plugins  
</li>
</ul>
<h2>Usage / example</h2>
<p>(1)<code>npm install smart-code</code></p>
<p>(2) Write command in your command promt <code> scode init demo(Directory Name)</code></p>
<br>
<p>
<img src="http://shivang.comuf.com/scode/demo_1.png" />
</p>
<br>
<p>(3) - For add js plugins in your projects(Templates) just go inside the directory <code>cd demo/html</code></p>
<br>
<p>
<img src="http://shivang.comuf.com/scode/demo_2.png"/>
</p>
<br>
<p>- Write command <code>scode plg reslid</code> now resposive slider add in your projects(Templates)</p>
<br>
<p><img src="http://shivang.comuf.com/scode/demo_3.png"/></p>
<p><img src="http://shivang.comuf.com/scode/demo_4.png"</p>
<br>
<p>(3) Upload Your own js plugins and use it <code>scode upload</code></p>
<br>
<p><img src="http://shivang.comuf.com/scode/demo_5.png" /></p>
<br>
<p>
<div class="highlight">
<pre class="editor editor-colors">
<div class="line">
<span class="text">Plugin Name :put a name of your plugin (required)</span>
<span class="text">css: put link of css only (optional)</span>
<span class="text">js: put link of js only (required)</span>
<span class="text">detail / url: put some detail about plugin or url of it</span>
<span class="text">script : $(function() {$("selector").plg({....});});</span>
</div>
</pre>
</div>

</p>
<h2>plugin Lists</h2>
<ul>
<li> -bootstrap    (Frontend FrameWork) </li>
<li> -owl          (Responsive Slider) </li>
<li> -datepicker   (Date Piker) </li>
<li> -reslid       (Responsive Slider) </li>
<li> -validate     (Form Validation) </li>
<li> -isotope      (Masonry Effect) </li>
<li> -ripf         (Ripple Effect) </li>
<li> -mscroll      (Styles Scrollbar) </li>
<li> -rangeSlider  (Range Slider) </li>
<li> -smoothscroll (Smooth Scrolling) </li>
<li> -fullpage     (Full Page) </li>
<li> -fancyselect  (Styles SelectBox) </li>
</ul>
<h2>Author</h2>
<p>Shivang Pokar <a href="https://gitlab.com/sivu503/scode/tree/master">https://gitlab.com/sivu503/scode</a></p>
    

